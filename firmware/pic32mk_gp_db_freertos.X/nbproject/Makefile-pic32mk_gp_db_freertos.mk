#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-pic32mk_gp_db_freertos.mk)" "nbproject/Makefile-local-pic32mk_gp_db_freertos.mk"
include nbproject/Makefile-local-pic32mk_gp_db_freertos.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=pic32mk_gp_db_freertos
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/pic32mk_gp_db_freertos.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/pic32mk_gp_db_freertos.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/config/pic32mk_gp_db_freertos/bsp/bsp.c ../src/config/pic32mk_gp_db_freertos/driver/usb/usbfs/src/drv_usbfs_device.c ../src/config/pic32mk_gp_db_freertos/driver/usb/usbfs/src/drv_usbfs.c ../src/config/pic32mk_gp_db_freertos/osal/osal_freertos.c ../src/config/pic32mk_gp_db_freertos/peripheral/clk/plib_clk.c ../src/config/pic32mk_gp_db_freertos/peripheral/evic/plib_evic.c ../src/config/pic32mk_gp_db_freertos/peripheral/gpio/plib_gpio.c ../src/config/pic32mk_gp_db_freertos/stdio/xc32_monitor.c ../src/config/pic32mk_gp_db_freertos/system/int/src/sys_int.c ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device.c ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device_cdc.c ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device_cdc_acm.c ../src/config/pic32mk_gp_db_freertos/initialization.c ../src/config/pic32mk_gp_db_freertos/interrupts.c ../src/config/pic32mk_gp_db_freertos/interrupts_a.S ../src/config/pic32mk_gp_db_freertos/exceptions.c ../src/config/pic32mk_gp_db_freertos/usb_device_init_data_0.c ../src/config/pic32mk_gp_db_freertos/freertos_hooks.c ../src/config/pic32mk_gp_db_freertos/tasks.c ../src/third_party/rtos/FreeRTOS/Source/portable/MemMang/heap_1.c ../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK/port.c ../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK/port_asm.S ../src/third_party/rtos/FreeRTOS/Source/croutine.c ../src/third_party/rtos/FreeRTOS/Source/list.c ../src/third_party/rtos/FreeRTOS/Source/queue.c ../src/third_party/rtos/FreeRTOS/Source/FreeRTOS_tasks.c ../src/third_party/rtos/FreeRTOS/Source/timers.c ../src/third_party/rtos/FreeRTOS/Source/event_groups.c ../src/third_party/rtos/FreeRTOS/Source/stream_buffer.c ../src/main.c ../src/app_freertos.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/21197704/bsp.o ${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o ${OBJECTDIR}/_ext/1200405861/drv_usbfs.o ${OBJECTDIR}/_ext/656741898/osal_freertos.o ${OBJECTDIR}/_ext/930826806/plib_clk.o ${OBJECTDIR}/_ext/1209070857/plib_evic.o ${OBJECTDIR}/_ext/1209017029/plib_gpio.o ${OBJECTDIR}/_ext/1119564418/xc32_monitor.o ${OBJECTDIR}/_ext/1542637557/sys_int.o ${OBJECTDIR}/_ext/392574830/usb_device.o ${OBJECTDIR}/_ext/392574830/usb_device_cdc.o ${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o ${OBJECTDIR}/_ext/1382896840/initialization.o ${OBJECTDIR}/_ext/1382896840/interrupts.o ${OBJECTDIR}/_ext/1382896840/interrupts_a.o ${OBJECTDIR}/_ext/1382896840/exceptions.o ${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o ${OBJECTDIR}/_ext/1382896840/freertos_hooks.o ${OBJECTDIR}/_ext/1382896840/tasks.o ${OBJECTDIR}/_ext/1665200909/heap_1.o ${OBJECTDIR}/_ext/951553261/port.o ${OBJECTDIR}/_ext/951553261/port_asm.o ${OBJECTDIR}/_ext/404212886/croutine.o ${OBJECTDIR}/_ext/404212886/list.o ${OBJECTDIR}/_ext/404212886/queue.o ${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o ${OBJECTDIR}/_ext/404212886/timers.o ${OBJECTDIR}/_ext/404212886/event_groups.o ${OBJECTDIR}/_ext/404212886/stream_buffer.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/app_freertos.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/21197704/bsp.o.d ${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o.d ${OBJECTDIR}/_ext/1200405861/drv_usbfs.o.d ${OBJECTDIR}/_ext/656741898/osal_freertos.o.d ${OBJECTDIR}/_ext/930826806/plib_clk.o.d ${OBJECTDIR}/_ext/1209070857/plib_evic.o.d ${OBJECTDIR}/_ext/1209017029/plib_gpio.o.d ${OBJECTDIR}/_ext/1119564418/xc32_monitor.o.d ${OBJECTDIR}/_ext/1542637557/sys_int.o.d ${OBJECTDIR}/_ext/392574830/usb_device.o.d ${OBJECTDIR}/_ext/392574830/usb_device_cdc.o.d ${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o.d ${OBJECTDIR}/_ext/1382896840/initialization.o.d ${OBJECTDIR}/_ext/1382896840/interrupts.o.d ${OBJECTDIR}/_ext/1382896840/interrupts_a.o.d ${OBJECTDIR}/_ext/1382896840/exceptions.o.d ${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o.d ${OBJECTDIR}/_ext/1382896840/freertos_hooks.o.d ${OBJECTDIR}/_ext/1382896840/tasks.o.d ${OBJECTDIR}/_ext/1665200909/heap_1.o.d ${OBJECTDIR}/_ext/951553261/port.o.d ${OBJECTDIR}/_ext/951553261/port_asm.o.d ${OBJECTDIR}/_ext/404212886/croutine.o.d ${OBJECTDIR}/_ext/404212886/list.o.d ${OBJECTDIR}/_ext/404212886/queue.o.d ${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o.d ${OBJECTDIR}/_ext/404212886/timers.o.d ${OBJECTDIR}/_ext/404212886/event_groups.o.d ${OBJECTDIR}/_ext/404212886/stream_buffer.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/1360937237/app_freertos.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/21197704/bsp.o ${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o ${OBJECTDIR}/_ext/1200405861/drv_usbfs.o ${OBJECTDIR}/_ext/656741898/osal_freertos.o ${OBJECTDIR}/_ext/930826806/plib_clk.o ${OBJECTDIR}/_ext/1209070857/plib_evic.o ${OBJECTDIR}/_ext/1209017029/plib_gpio.o ${OBJECTDIR}/_ext/1119564418/xc32_monitor.o ${OBJECTDIR}/_ext/1542637557/sys_int.o ${OBJECTDIR}/_ext/392574830/usb_device.o ${OBJECTDIR}/_ext/392574830/usb_device_cdc.o ${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o ${OBJECTDIR}/_ext/1382896840/initialization.o ${OBJECTDIR}/_ext/1382896840/interrupts.o ${OBJECTDIR}/_ext/1382896840/interrupts_a.o ${OBJECTDIR}/_ext/1382896840/exceptions.o ${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o ${OBJECTDIR}/_ext/1382896840/freertos_hooks.o ${OBJECTDIR}/_ext/1382896840/tasks.o ${OBJECTDIR}/_ext/1665200909/heap_1.o ${OBJECTDIR}/_ext/951553261/port.o ${OBJECTDIR}/_ext/951553261/port_asm.o ${OBJECTDIR}/_ext/404212886/croutine.o ${OBJECTDIR}/_ext/404212886/list.o ${OBJECTDIR}/_ext/404212886/queue.o ${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o ${OBJECTDIR}/_ext/404212886/timers.o ${OBJECTDIR}/_ext/404212886/event_groups.o ${OBJECTDIR}/_ext/404212886/stream_buffer.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/app_freertos.o

# Source Files
SOURCEFILES=../src/config/pic32mk_gp_db_freertos/bsp/bsp.c ../src/config/pic32mk_gp_db_freertos/driver/usb/usbfs/src/drv_usbfs_device.c ../src/config/pic32mk_gp_db_freertos/driver/usb/usbfs/src/drv_usbfs.c ../src/config/pic32mk_gp_db_freertos/osal/osal_freertos.c ../src/config/pic32mk_gp_db_freertos/peripheral/clk/plib_clk.c ../src/config/pic32mk_gp_db_freertos/peripheral/evic/plib_evic.c ../src/config/pic32mk_gp_db_freertos/peripheral/gpio/plib_gpio.c ../src/config/pic32mk_gp_db_freertos/stdio/xc32_monitor.c ../src/config/pic32mk_gp_db_freertos/system/int/src/sys_int.c ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device.c ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device_cdc.c ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device_cdc_acm.c ../src/config/pic32mk_gp_db_freertos/initialization.c ../src/config/pic32mk_gp_db_freertos/interrupts.c ../src/config/pic32mk_gp_db_freertos/interrupts_a.S ../src/config/pic32mk_gp_db_freertos/exceptions.c ../src/config/pic32mk_gp_db_freertos/usb_device_init_data_0.c ../src/config/pic32mk_gp_db_freertos/freertos_hooks.c ../src/config/pic32mk_gp_db_freertos/tasks.c ../src/third_party/rtos/FreeRTOS/Source/portable/MemMang/heap_1.c ../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK/port.c ../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK/port_asm.S ../src/third_party/rtos/FreeRTOS/Source/croutine.c ../src/third_party/rtos/FreeRTOS/Source/list.c ../src/third_party/rtos/FreeRTOS/Source/queue.c ../src/third_party/rtos/FreeRTOS/Source/FreeRTOS_tasks.c ../src/third_party/rtos/FreeRTOS/Source/timers.c ../src/third_party/rtos/FreeRTOS/Source/event_groups.c ../src/third_party/rtos/FreeRTOS/Source/stream_buffer.c ../src/main.c ../src/app_freertos.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-pic32mk_gp_db_freertos.mk dist/${CND_CONF}/${IMAGE_TYPE}/pic32mk_gp_db_freertos.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MK1024GPE100
MP_LINKER_FILE_OPTION=,--script="..\src\config\pic32mk_gp_db_freertos\p32MK1024GPE100.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1382896840/interrupts_a.o: ../src/config/pic32mk_gp_db_freertos/interrupts_a.S  .generated_files/flags/pic32mk_gp_db_freertos/4dedd29e08bb66eeaf3ba66a0878bfe75b67f469 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/interrupts_a.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/interrupts_a.o 
	@${RM} ${OBJECTDIR}/_ext/1382896840/interrupts_a.o.ok ${OBJECTDIR}/_ext/1382896840/interrupts_a.o.err 
	${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../src/config/pic32mk_gp_db_freertos" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -MMD -MF "${OBJECTDIR}/_ext/1382896840/interrupts_a.o.d"  -o ${OBJECTDIR}/_ext/1382896840/interrupts_a.o ../src/config/pic32mk_gp_db_freertos/interrupts_a.S  -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/1382896840/interrupts_a.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,-I"../src/config/pic32mk_gp_db_freertos" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -mdfp="${DFP_DIR}"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1382896840/interrupts_a.o.d" "${OBJECTDIR}/_ext/1382896840/interrupts_a.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/951553261/port_asm.o: ../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK/port_asm.S  .generated_files/flags/pic32mk_gp_db_freertos/8e8f2da5f512fd46a16c67b53396c1084858936b .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/951553261" 
	@${RM} ${OBJECTDIR}/_ext/951553261/port_asm.o.d 
	@${RM} ${OBJECTDIR}/_ext/951553261/port_asm.o 
	@${RM} ${OBJECTDIR}/_ext/951553261/port_asm.o.ok ${OBJECTDIR}/_ext/951553261/port_asm.o.err 
	${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../src/config/pic32mk_gp_db_freertos" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -MMD -MF "${OBJECTDIR}/_ext/951553261/port_asm.o.d"  -o ${OBJECTDIR}/_ext/951553261/port_asm.o ../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK/port_asm.S  -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/951553261/port_asm.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,-I"../src/config/pic32mk_gp_db_freertos" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -mdfp="${DFP_DIR}"
	@${FIXDEPS} "${OBJECTDIR}/_ext/951553261/port_asm.o.d" "${OBJECTDIR}/_ext/951553261/port_asm.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/_ext/1382896840/interrupts_a.o: ../src/config/pic32mk_gp_db_freertos/interrupts_a.S  .generated_files/flags/pic32mk_gp_db_freertos/2791cbc7e27f3269a5380fd5b00fb129ad6493fd .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/interrupts_a.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/interrupts_a.o 
	@${RM} ${OBJECTDIR}/_ext/1382896840/interrupts_a.o.ok ${OBJECTDIR}/_ext/1382896840/interrupts_a.o.err 
	${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../src/config/pic32mk_gp_db_freertos" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -MMD -MF "${OBJECTDIR}/_ext/1382896840/interrupts_a.o.d"  -o ${OBJECTDIR}/_ext/1382896840/interrupts_a.o ../src/config/pic32mk_gp_db_freertos/interrupts_a.S  -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/1382896840/interrupts_a.o.asm.d",--gdwarf-2,-I"../src/config/pic32mk_gp_db_freertos" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -mdfp="${DFP_DIR}"
	@${FIXDEPS} "${OBJECTDIR}/_ext/1382896840/interrupts_a.o.d" "${OBJECTDIR}/_ext/1382896840/interrupts_a.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/951553261/port_asm.o: ../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK/port_asm.S  .generated_files/flags/pic32mk_gp_db_freertos/7efea0d9d9b749076976b9d9542b0816b4ee4da6 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/951553261" 
	@${RM} ${OBJECTDIR}/_ext/951553261/port_asm.o.d 
	@${RM} ${OBJECTDIR}/_ext/951553261/port_asm.o 
	@${RM} ${OBJECTDIR}/_ext/951553261/port_asm.o.ok ${OBJECTDIR}/_ext/951553261/port_asm.o.err 
	${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../src/config/pic32mk_gp_db_freertos" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -MMD -MF "${OBJECTDIR}/_ext/951553261/port_asm.o.d"  -o ${OBJECTDIR}/_ext/951553261/port_asm.o ../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK/port_asm.S  -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/951553261/port_asm.o.asm.d",--gdwarf-2,-I"../src/config/pic32mk_gp_db_freertos" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -mdfp="${DFP_DIR}"
	@${FIXDEPS} "${OBJECTDIR}/_ext/951553261/port_asm.o.d" "${OBJECTDIR}/_ext/951553261/port_asm.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/21197704/bsp.o: ../src/config/pic32mk_gp_db_freertos/bsp/bsp.c  .generated_files/flags/pic32mk_gp_db_freertos/8375f444cd40c0f54cc462b6076b6cf1c25b2694 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/21197704" 
	@${RM} ${OBJECTDIR}/_ext/21197704/bsp.o.d 
	@${RM} ${OBJECTDIR}/_ext/21197704/bsp.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/21197704/bsp.o.d" -o ${OBJECTDIR}/_ext/21197704/bsp.o ../src/config/pic32mk_gp_db_freertos/bsp/bsp.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o: ../src/config/pic32mk_gp_db_freertos/driver/usb/usbfs/src/drv_usbfs_device.c  .generated_files/flags/pic32mk_gp_db_freertos/a21d9f023e4e3f87ad02fe05f2a05b3d385f1e85 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1200405861" 
	@${RM} ${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o.d" -o ${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o ../src/config/pic32mk_gp_db_freertos/driver/usb/usbfs/src/drv_usbfs_device.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1200405861/drv_usbfs.o: ../src/config/pic32mk_gp_db_freertos/driver/usb/usbfs/src/drv_usbfs.c  .generated_files/flags/pic32mk_gp_db_freertos/1d2069ee333d4934a7a51cfc19bde4197034811a .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1200405861" 
	@${RM} ${OBJECTDIR}/_ext/1200405861/drv_usbfs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1200405861/drv_usbfs.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1200405861/drv_usbfs.o.d" -o ${OBJECTDIR}/_ext/1200405861/drv_usbfs.o ../src/config/pic32mk_gp_db_freertos/driver/usb/usbfs/src/drv_usbfs.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/656741898/osal_freertos.o: ../src/config/pic32mk_gp_db_freertos/osal/osal_freertos.c  .generated_files/flags/pic32mk_gp_db_freertos/2afc72e4bafba08ca55748994ad910bb36474974 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/656741898" 
	@${RM} ${OBJECTDIR}/_ext/656741898/osal_freertos.o.d 
	@${RM} ${OBJECTDIR}/_ext/656741898/osal_freertos.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/656741898/osal_freertos.o.d" -o ${OBJECTDIR}/_ext/656741898/osal_freertos.o ../src/config/pic32mk_gp_db_freertos/osal/osal_freertos.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/930826806/plib_clk.o: ../src/config/pic32mk_gp_db_freertos/peripheral/clk/plib_clk.c  .generated_files/flags/pic32mk_gp_db_freertos/f19d1ddc443bfa350d36ba2c8e6c86aa2ac39043 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/930826806" 
	@${RM} ${OBJECTDIR}/_ext/930826806/plib_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/930826806/plib_clk.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/930826806/plib_clk.o.d" -o ${OBJECTDIR}/_ext/930826806/plib_clk.o ../src/config/pic32mk_gp_db_freertos/peripheral/clk/plib_clk.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1209070857/plib_evic.o: ../src/config/pic32mk_gp_db_freertos/peripheral/evic/plib_evic.c  .generated_files/flags/pic32mk_gp_db_freertos/1f78cf39b2e9b043af33f5551a42208712d167c9 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1209070857" 
	@${RM} ${OBJECTDIR}/_ext/1209070857/plib_evic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1209070857/plib_evic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1209070857/plib_evic.o.d" -o ${OBJECTDIR}/_ext/1209070857/plib_evic.o ../src/config/pic32mk_gp_db_freertos/peripheral/evic/plib_evic.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1209017029/plib_gpio.o: ../src/config/pic32mk_gp_db_freertos/peripheral/gpio/plib_gpio.c  .generated_files/flags/pic32mk_gp_db_freertos/82c5557340d26cc2905d4bd07cd85664377a702c .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1209017029" 
	@${RM} ${OBJECTDIR}/_ext/1209017029/plib_gpio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1209017029/plib_gpio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1209017029/plib_gpio.o.d" -o ${OBJECTDIR}/_ext/1209017029/plib_gpio.o ../src/config/pic32mk_gp_db_freertos/peripheral/gpio/plib_gpio.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1119564418/xc32_monitor.o: ../src/config/pic32mk_gp_db_freertos/stdio/xc32_monitor.c  .generated_files/flags/pic32mk_gp_db_freertos/7c5d46db8715603e0faac7ea2677d8c4c6178cdb .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1119564418" 
	@${RM} ${OBJECTDIR}/_ext/1119564418/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/1119564418/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1119564418/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/1119564418/xc32_monitor.o ../src/config/pic32mk_gp_db_freertos/stdio/xc32_monitor.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1542637557/sys_int.o: ../src/config/pic32mk_gp_db_freertos/system/int/src/sys_int.c  .generated_files/flags/pic32mk_gp_db_freertos/d12e4e6e0654f2439a38f46554de2cdb1ffdfd5d .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1542637557" 
	@${RM} ${OBJECTDIR}/_ext/1542637557/sys_int.o.d 
	@${RM} ${OBJECTDIR}/_ext/1542637557/sys_int.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1542637557/sys_int.o.d" -o ${OBJECTDIR}/_ext/1542637557/sys_int.o ../src/config/pic32mk_gp_db_freertos/system/int/src/sys_int.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/392574830/usb_device.o: ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device.c  .generated_files/flags/pic32mk_gp_db_freertos/bd7bb3892382f1ab921e5819bf469ce4c2b0860c .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/392574830" 
	@${RM} ${OBJECTDIR}/_ext/392574830/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/392574830/usb_device.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/392574830/usb_device.o.d" -o ${OBJECTDIR}/_ext/392574830/usb_device.o ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/392574830/usb_device_cdc.o: ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device_cdc.c  .generated_files/flags/pic32mk_gp_db_freertos/ce57dbfb47f34bb0a90be584206e5d5c0588a3b .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/392574830" 
	@${RM} ${OBJECTDIR}/_ext/392574830/usb_device_cdc.o.d 
	@${RM} ${OBJECTDIR}/_ext/392574830/usb_device_cdc.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/392574830/usb_device_cdc.o.d" -o ${OBJECTDIR}/_ext/392574830/usb_device_cdc.o ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device_cdc.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o: ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device_cdc_acm.c  .generated_files/flags/pic32mk_gp_db_freertos/fdb222fccab01f99dd4f0f38cc56d73bd064af81 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/392574830" 
	@${RM} ${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o.d 
	@${RM} ${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o.d" -o ${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device_cdc_acm.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1382896840/initialization.o: ../src/config/pic32mk_gp_db_freertos/initialization.c  .generated_files/flags/pic32mk_gp_db_freertos/4085be48661503de74c9fc7dd9c12ae22c7d8d67 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1382896840/initialization.o.d" -o ${OBJECTDIR}/_ext/1382896840/initialization.o ../src/config/pic32mk_gp_db_freertos/initialization.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1382896840/interrupts.o: ../src/config/pic32mk_gp_db_freertos/interrupts.c  .generated_files/flags/pic32mk_gp_db_freertos/c7b01e310778fa4d937b53a8ce51c036dc592f87 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1382896840/interrupts.o.d" -o ${OBJECTDIR}/_ext/1382896840/interrupts.o ../src/config/pic32mk_gp_db_freertos/interrupts.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1382896840/exceptions.o: ../src/config/pic32mk_gp_db_freertos/exceptions.c  .generated_files/flags/pic32mk_gp_db_freertos/d504eeafa86ab993825015d367c72fd08f56e4b1 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1382896840/exceptions.o.d" -o ${OBJECTDIR}/_ext/1382896840/exceptions.o ../src/config/pic32mk_gp_db_freertos/exceptions.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o: ../src/config/pic32mk_gp_db_freertos/usb_device_init_data_0.c  .generated_files/flags/pic32mk_gp_db_freertos/8a19c41991296501c10218c3fcb7eac94ae7929a .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o.d" -o ${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o ../src/config/pic32mk_gp_db_freertos/usb_device_init_data_0.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1382896840/freertos_hooks.o: ../src/config/pic32mk_gp_db_freertos/freertos_hooks.c  .generated_files/flags/pic32mk_gp_db_freertos/311a8f1d1d774facd4a8492c8baf058e52bdb7fb .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/freertos_hooks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/freertos_hooks.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1382896840/freertos_hooks.o.d" -o ${OBJECTDIR}/_ext/1382896840/freertos_hooks.o ../src/config/pic32mk_gp_db_freertos/freertos_hooks.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1382896840/tasks.o: ../src/config/pic32mk_gp_db_freertos/tasks.c  .generated_files/flags/pic32mk_gp_db_freertos/97b9f1a8655a2f737d97622adca6cb593aa6ebb4 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/tasks.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1382896840/tasks.o.d" -o ${OBJECTDIR}/_ext/1382896840/tasks.o ../src/config/pic32mk_gp_db_freertos/tasks.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1665200909/heap_1.o: ../src/third_party/rtos/FreeRTOS/Source/portable/MemMang/heap_1.c  .generated_files/flags/pic32mk_gp_db_freertos/d796ac881a5395b62a50c739f741fe11cf95646a .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1665200909" 
	@${RM} ${OBJECTDIR}/_ext/1665200909/heap_1.o.d 
	@${RM} ${OBJECTDIR}/_ext/1665200909/heap_1.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1665200909/heap_1.o.d" -o ${OBJECTDIR}/_ext/1665200909/heap_1.o ../src/third_party/rtos/FreeRTOS/Source/portable/MemMang/heap_1.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/951553261/port.o: ../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK/port.c  .generated_files/flags/pic32mk_gp_db_freertos/4565a2da41490e7261e57429d3c268169853d52b .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/951553261" 
	@${RM} ${OBJECTDIR}/_ext/951553261/port.o.d 
	@${RM} ${OBJECTDIR}/_ext/951553261/port.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/951553261/port.o.d" -o ${OBJECTDIR}/_ext/951553261/port.o ../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK/port.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/croutine.o: ../src/third_party/rtos/FreeRTOS/Source/croutine.c  .generated_files/flags/pic32mk_gp_db_freertos/4ccddab2530b4b8a1e4b623d29e2d6bf5dbc9808 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/croutine.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/croutine.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/croutine.o.d" -o ${OBJECTDIR}/_ext/404212886/croutine.o ../src/third_party/rtos/FreeRTOS/Source/croutine.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/list.o: ../src/third_party/rtos/FreeRTOS/Source/list.c  .generated_files/flags/pic32mk_gp_db_freertos/bc4d19f683166ff42ff31d7750848e57619eddce .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/list.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/list.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/list.o.d" -o ${OBJECTDIR}/_ext/404212886/list.o ../src/third_party/rtos/FreeRTOS/Source/list.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/queue.o: ../src/third_party/rtos/FreeRTOS/Source/queue.c  .generated_files/flags/pic32mk_gp_db_freertos/93537136e1728b85ec2342d09f9f137695b7e379 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/queue.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/queue.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/queue.o.d" -o ${OBJECTDIR}/_ext/404212886/queue.o ../src/third_party/rtos/FreeRTOS/Source/queue.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o: ../src/third_party/rtos/FreeRTOS/Source/FreeRTOS_tasks.c  .generated_files/flags/pic32mk_gp_db_freertos/b5a78908f245f776795b272661fd934c97a4b760 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o.d" -o ${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o ../src/third_party/rtos/FreeRTOS/Source/FreeRTOS_tasks.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/timers.o: ../src/third_party/rtos/FreeRTOS/Source/timers.c  .generated_files/flags/pic32mk_gp_db_freertos/87bc4c1a7f4b0a728adddb34275a045943f78cb5 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/timers.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/timers.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/timers.o.d" -o ${OBJECTDIR}/_ext/404212886/timers.o ../src/third_party/rtos/FreeRTOS/Source/timers.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/event_groups.o: ../src/third_party/rtos/FreeRTOS/Source/event_groups.c  .generated_files/flags/pic32mk_gp_db_freertos/14f284ecd77ddced6a3d018d2b5665dfc5317f6 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/event_groups.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/event_groups.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/event_groups.o.d" -o ${OBJECTDIR}/_ext/404212886/event_groups.o ../src/third_party/rtos/FreeRTOS/Source/event_groups.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/stream_buffer.o: ../src/third_party/rtos/FreeRTOS/Source/stream_buffer.c  .generated_files/flags/pic32mk_gp_db_freertos/dd9a10ebe11d5f33c9a76c78602689d2fc0740a5 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/stream_buffer.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/stream_buffer.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/stream_buffer.o.d" -o ${OBJECTDIR}/_ext/404212886/stream_buffer.o ../src/third_party/rtos/FreeRTOS/Source/stream_buffer.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/flags/pic32mk_gp_db_freertos/bd532069ed37d5ee8401034f98fc0d20c5cf26e5 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/app_freertos.o: ../src/app_freertos.c  .generated_files/flags/pic32mk_gp_db_freertos/c03fc42d7950bd056af2cb1d5ed9aca5f9d629f3 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app_freertos.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app_freertos.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/app_freertos.o.d" -o ${OBJECTDIR}/_ext/1360937237/app_freertos.o ../src/app_freertos.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
else
${OBJECTDIR}/_ext/21197704/bsp.o: ../src/config/pic32mk_gp_db_freertos/bsp/bsp.c  .generated_files/flags/pic32mk_gp_db_freertos/d0d365dc0f1d59e1ddc7b4f697319d5ab58afebb .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/21197704" 
	@${RM} ${OBJECTDIR}/_ext/21197704/bsp.o.d 
	@${RM} ${OBJECTDIR}/_ext/21197704/bsp.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/21197704/bsp.o.d" -o ${OBJECTDIR}/_ext/21197704/bsp.o ../src/config/pic32mk_gp_db_freertos/bsp/bsp.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o: ../src/config/pic32mk_gp_db_freertos/driver/usb/usbfs/src/drv_usbfs_device.c  .generated_files/flags/pic32mk_gp_db_freertos/c2e6f62f114cd56d9ba20e6d61c406438f66a11f .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1200405861" 
	@${RM} ${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o.d" -o ${OBJECTDIR}/_ext/1200405861/drv_usbfs_device.o ../src/config/pic32mk_gp_db_freertos/driver/usb/usbfs/src/drv_usbfs_device.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1200405861/drv_usbfs.o: ../src/config/pic32mk_gp_db_freertos/driver/usb/usbfs/src/drv_usbfs.c  .generated_files/flags/pic32mk_gp_db_freertos/5c7f6dfc7335ef7e5ab6780e6e353d473efc45d5 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1200405861" 
	@${RM} ${OBJECTDIR}/_ext/1200405861/drv_usbfs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1200405861/drv_usbfs.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1200405861/drv_usbfs.o.d" -o ${OBJECTDIR}/_ext/1200405861/drv_usbfs.o ../src/config/pic32mk_gp_db_freertos/driver/usb/usbfs/src/drv_usbfs.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/656741898/osal_freertos.o: ../src/config/pic32mk_gp_db_freertos/osal/osal_freertos.c  .generated_files/flags/pic32mk_gp_db_freertos/a85e966c8733ed61a3bac45401c1970fc35b154f .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/656741898" 
	@${RM} ${OBJECTDIR}/_ext/656741898/osal_freertos.o.d 
	@${RM} ${OBJECTDIR}/_ext/656741898/osal_freertos.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/656741898/osal_freertos.o.d" -o ${OBJECTDIR}/_ext/656741898/osal_freertos.o ../src/config/pic32mk_gp_db_freertos/osal/osal_freertos.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/930826806/plib_clk.o: ../src/config/pic32mk_gp_db_freertos/peripheral/clk/plib_clk.c  .generated_files/flags/pic32mk_gp_db_freertos/909faf606bb3f9e75dfa8ada96ae5646c091dd6a .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/930826806" 
	@${RM} ${OBJECTDIR}/_ext/930826806/plib_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/930826806/plib_clk.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/930826806/plib_clk.o.d" -o ${OBJECTDIR}/_ext/930826806/plib_clk.o ../src/config/pic32mk_gp_db_freertos/peripheral/clk/plib_clk.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1209070857/plib_evic.o: ../src/config/pic32mk_gp_db_freertos/peripheral/evic/plib_evic.c  .generated_files/flags/pic32mk_gp_db_freertos/d48196a00091a23f315d7e320d96f7a6c7aea75a .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1209070857" 
	@${RM} ${OBJECTDIR}/_ext/1209070857/plib_evic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1209070857/plib_evic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1209070857/plib_evic.o.d" -o ${OBJECTDIR}/_ext/1209070857/plib_evic.o ../src/config/pic32mk_gp_db_freertos/peripheral/evic/plib_evic.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1209017029/plib_gpio.o: ../src/config/pic32mk_gp_db_freertos/peripheral/gpio/plib_gpio.c  .generated_files/flags/pic32mk_gp_db_freertos/f6c20053e03b2c7d2fb4fa0e11a2a1c84ef6334e .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1209017029" 
	@${RM} ${OBJECTDIR}/_ext/1209017029/plib_gpio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1209017029/plib_gpio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1209017029/plib_gpio.o.d" -o ${OBJECTDIR}/_ext/1209017029/plib_gpio.o ../src/config/pic32mk_gp_db_freertos/peripheral/gpio/plib_gpio.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1119564418/xc32_monitor.o: ../src/config/pic32mk_gp_db_freertos/stdio/xc32_monitor.c  .generated_files/flags/pic32mk_gp_db_freertos/c5a2ec5e9105e7c688d39c8909d6ce50bccdfe30 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1119564418" 
	@${RM} ${OBJECTDIR}/_ext/1119564418/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/1119564418/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1119564418/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/1119564418/xc32_monitor.o ../src/config/pic32mk_gp_db_freertos/stdio/xc32_monitor.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1542637557/sys_int.o: ../src/config/pic32mk_gp_db_freertos/system/int/src/sys_int.c  .generated_files/flags/pic32mk_gp_db_freertos/a6c261d76a11ab1e4ff78e9a3c41cff944e2c6e5 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1542637557" 
	@${RM} ${OBJECTDIR}/_ext/1542637557/sys_int.o.d 
	@${RM} ${OBJECTDIR}/_ext/1542637557/sys_int.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1542637557/sys_int.o.d" -o ${OBJECTDIR}/_ext/1542637557/sys_int.o ../src/config/pic32mk_gp_db_freertos/system/int/src/sys_int.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/392574830/usb_device.o: ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device.c  .generated_files/flags/pic32mk_gp_db_freertos/a519ae90e20bdb6b80c1c7d027d95beff8a3baf7 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/392574830" 
	@${RM} ${OBJECTDIR}/_ext/392574830/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/392574830/usb_device.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/392574830/usb_device.o.d" -o ${OBJECTDIR}/_ext/392574830/usb_device.o ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/392574830/usb_device_cdc.o: ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device_cdc.c  .generated_files/flags/pic32mk_gp_db_freertos/38e5cbee4f097aa9ad17527091eee9c1d6df99d6 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/392574830" 
	@${RM} ${OBJECTDIR}/_ext/392574830/usb_device_cdc.o.d 
	@${RM} ${OBJECTDIR}/_ext/392574830/usb_device_cdc.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/392574830/usb_device_cdc.o.d" -o ${OBJECTDIR}/_ext/392574830/usb_device_cdc.o ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device_cdc.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o: ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device_cdc_acm.c  .generated_files/flags/pic32mk_gp_db_freertos/15c049a0264011946120f9b2ad9dd2b8ceec8fc6 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/392574830" 
	@${RM} ${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o.d 
	@${RM} ${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o.d" -o ${OBJECTDIR}/_ext/392574830/usb_device_cdc_acm.o ../src/config/pic32mk_gp_db_freertos/usb/src/usb_device_cdc_acm.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1382896840/initialization.o: ../src/config/pic32mk_gp_db_freertos/initialization.c  .generated_files/flags/pic32mk_gp_db_freertos/698f1e988c06972f8c5b898921ca2b18f9844ba7 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1382896840/initialization.o.d" -o ${OBJECTDIR}/_ext/1382896840/initialization.o ../src/config/pic32mk_gp_db_freertos/initialization.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1382896840/interrupts.o: ../src/config/pic32mk_gp_db_freertos/interrupts.c  .generated_files/flags/pic32mk_gp_db_freertos/e3b435f924ce8f4b69c4b16841448eaf614c113b .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1382896840/interrupts.o.d" -o ${OBJECTDIR}/_ext/1382896840/interrupts.o ../src/config/pic32mk_gp_db_freertos/interrupts.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1382896840/exceptions.o: ../src/config/pic32mk_gp_db_freertos/exceptions.c  .generated_files/flags/pic32mk_gp_db_freertos/8dc0b40726e1ddf8a3e3827779b14e3b857ce00a .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1382896840/exceptions.o.d" -o ${OBJECTDIR}/_ext/1382896840/exceptions.o ../src/config/pic32mk_gp_db_freertos/exceptions.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o: ../src/config/pic32mk_gp_db_freertos/usb_device_init_data_0.c  .generated_files/flags/pic32mk_gp_db_freertos/2e195e1c1425b339187805f0bf8edf43d78c8799 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o.d" -o ${OBJECTDIR}/_ext/1382896840/usb_device_init_data_0.o ../src/config/pic32mk_gp_db_freertos/usb_device_init_data_0.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1382896840/freertos_hooks.o: ../src/config/pic32mk_gp_db_freertos/freertos_hooks.c  .generated_files/flags/pic32mk_gp_db_freertos/7472bb9e634ca86ea2d96c848cf11e83a26afe4a .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/freertos_hooks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/freertos_hooks.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1382896840/freertos_hooks.o.d" -o ${OBJECTDIR}/_ext/1382896840/freertos_hooks.o ../src/config/pic32mk_gp_db_freertos/freertos_hooks.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1382896840/tasks.o: ../src/config/pic32mk_gp_db_freertos/tasks.c  .generated_files/flags/pic32mk_gp_db_freertos/d589a5b6754c95c42fe443f56efb50971a14b5b1 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1382896840" 
	@${RM} ${OBJECTDIR}/_ext/1382896840/tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1382896840/tasks.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1382896840/tasks.o.d" -o ${OBJECTDIR}/_ext/1382896840/tasks.o ../src/config/pic32mk_gp_db_freertos/tasks.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1665200909/heap_1.o: ../src/third_party/rtos/FreeRTOS/Source/portable/MemMang/heap_1.c  .generated_files/flags/pic32mk_gp_db_freertos/ce86463e4c61bfa77da5c61e9d364ee3ba796281 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1665200909" 
	@${RM} ${OBJECTDIR}/_ext/1665200909/heap_1.o.d 
	@${RM} ${OBJECTDIR}/_ext/1665200909/heap_1.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1665200909/heap_1.o.d" -o ${OBJECTDIR}/_ext/1665200909/heap_1.o ../src/third_party/rtos/FreeRTOS/Source/portable/MemMang/heap_1.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/951553261/port.o: ../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK/port.c  .generated_files/flags/pic32mk_gp_db_freertos/dcb788debdbabfe4c14c825a322e6c0d157a3fb1 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/951553261" 
	@${RM} ${OBJECTDIR}/_ext/951553261/port.o.d 
	@${RM} ${OBJECTDIR}/_ext/951553261/port.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/951553261/port.o.d" -o ${OBJECTDIR}/_ext/951553261/port.o ../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK/port.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/croutine.o: ../src/third_party/rtos/FreeRTOS/Source/croutine.c  .generated_files/flags/pic32mk_gp_db_freertos/84701047dd0f9118739ac66761993665d7c1a321 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/croutine.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/croutine.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/croutine.o.d" -o ${OBJECTDIR}/_ext/404212886/croutine.o ../src/third_party/rtos/FreeRTOS/Source/croutine.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/list.o: ../src/third_party/rtos/FreeRTOS/Source/list.c  .generated_files/flags/pic32mk_gp_db_freertos/1b06ae30c43043c433748a840ca3d11c2145773e .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/list.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/list.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/list.o.d" -o ${OBJECTDIR}/_ext/404212886/list.o ../src/third_party/rtos/FreeRTOS/Source/list.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/queue.o: ../src/third_party/rtos/FreeRTOS/Source/queue.c  .generated_files/flags/pic32mk_gp_db_freertos/79041f5d4387abff21ac8f08f1b5d7d92488ebc2 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/queue.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/queue.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/queue.o.d" -o ${OBJECTDIR}/_ext/404212886/queue.o ../src/third_party/rtos/FreeRTOS/Source/queue.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o: ../src/third_party/rtos/FreeRTOS/Source/FreeRTOS_tasks.c  .generated_files/flags/pic32mk_gp_db_freertos/838df37c0969b37d2e9e3ac1a787f525851db579 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o.d" -o ${OBJECTDIR}/_ext/404212886/FreeRTOS_tasks.o ../src/third_party/rtos/FreeRTOS/Source/FreeRTOS_tasks.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/timers.o: ../src/third_party/rtos/FreeRTOS/Source/timers.c  .generated_files/flags/pic32mk_gp_db_freertos/3894933f74f1050079399842bbb7cbfdcdce8129 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/timers.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/timers.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/timers.o.d" -o ${OBJECTDIR}/_ext/404212886/timers.o ../src/third_party/rtos/FreeRTOS/Source/timers.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/event_groups.o: ../src/third_party/rtos/FreeRTOS/Source/event_groups.c  .generated_files/flags/pic32mk_gp_db_freertos/e64f3af12627fbc3a6d0a50ae8dff31f731cb0e7 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/event_groups.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/event_groups.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/event_groups.o.d" -o ${OBJECTDIR}/_ext/404212886/event_groups.o ../src/third_party/rtos/FreeRTOS/Source/event_groups.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/404212886/stream_buffer.o: ../src/third_party/rtos/FreeRTOS/Source/stream_buffer.c  .generated_files/flags/pic32mk_gp_db_freertos/a3cd7cafa49be791002d9346b5898dc8bf21bab7 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/404212886" 
	@${RM} ${OBJECTDIR}/_ext/404212886/stream_buffer.o.d 
	@${RM} ${OBJECTDIR}/_ext/404212886/stream_buffer.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/404212886/stream_buffer.o.d" -o ${OBJECTDIR}/_ext/404212886/stream_buffer.o ../src/third_party/rtos/FreeRTOS/Source/stream_buffer.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/flags/pic32mk_gp_db_freertos/48f50bed3aa8fb3690de640cdecf275b76f14664 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/app_freertos.o: ../src/app_freertos.c  .generated_files/flags/pic32mk_gp_db_freertos/e9c6ba60fbc12b9c4f04a35d617d83d6cf84a08 .generated_files/flags/pic32mk_gp_db_freertos/73e2fb0cc4ef2071fe56806e6ef40c38cf6349ef
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app_freertos.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app_freertos.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -fno-common -I"../src" -I"../src/config/pic32mk_gp_db_freertos" -I"../src/packs/PIC32MK1024GPE100_DFP" -I"../src/third_party/rtos/FreeRTOS/Source/include" -I"../src/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MK" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/app_freertos.o.d" -o ${OBJECTDIR}/_ext/1360937237/app_freertos.o ../src/app_freertos.c    -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/pic32mk_gp_db_freertos.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../src/config/pic32mk_gp_db_freertos/p32MK1024GPE100.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g   -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/pic32mk_gp_db_freertos.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x0:0x36F   -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=_min_heap_size=512,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/pic32mk_gp_db_freertos.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../src/config/pic32mk_gp_db_freertos/p32MK1024GPE100.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/pic32mk_gp_db_freertos.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_pic32mk_gp_db_freertos=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=512,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/pic32mk_gp_db_freertos.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/pic32mk_gp_db_freertos
	${RM} -r dist/pic32mk_gp_db_freertos

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
